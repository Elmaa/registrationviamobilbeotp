package mobileregistration.otp.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mobileregistration.otp.data.OtpModel;
import mobileregistration.otp.data.OtpModelDAO;
import mobileregistration.otp.service.OtpService;
import mobileregistration.otp.utility.OtpUtilityService;

@Service
public class OtpServiceImpl implements OtpService{

	@Autowired
	private OtpModelDAO userModelDao;

	@Autowired
	private OtpUtilityService otpUtilityService;
	
	@Override
	public String saveOtp(String mobileNumber) {
		
		if(otpUtilityService.getOtp(mobileNumber).isPresent()) {
			String result = "Verification code is already exist, you can try 3 minutes later!";
			return result;
		}
		
		Integer otp = otpUtilityService.generateOTP(mobileNumber);

		OtpModel otpModel = new OtpModel(mobileNumber, otp);
		userModelDao.save(otpModel);

		return otp.toString();
	}

	@Override
	public Boolean otpValidation(String mobileNumber, Integer otp) {
		
		Optional<Integer> serverOtp = otpUtilityService.getOtp(mobileNumber);
		if(serverOtp.isPresent() && serverOtp.get().equals(otp)) {
			otpUtilityService.clearOTP(mobileNumber);
			return true;
		}
		else if(serverOtp.get() != otp) {
			otpUtilityService.clearOTP(mobileNumber);
		}
		return false;
	}

}
