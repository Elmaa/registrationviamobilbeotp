package mobileregistration.otp.service;

public interface OtpService {

	String saveOtp(String mobileNumber);
	Boolean otpValidation(String mobileNumber, Integer otp);
}
