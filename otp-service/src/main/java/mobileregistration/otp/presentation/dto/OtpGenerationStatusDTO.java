package mobileregistration.otp.presentation.dto;

public class OtpGenerationStatusDTO {

	private String status;

	public OtpGenerationStatusDTO(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
