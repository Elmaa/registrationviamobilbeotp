package mobileregistration.otp.presentation.dto;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

public class MobileNumberDTO {

	@NotBlank
	@Length(min=11, max=11, message = "Mobile number should be 11 characters")
	private String mobileNumber;


	public MobileNumberDTO() {
		super();
	}

	public MobileNumberDTO(String mobileNumber) {
		super();
		this.mobileNumber = mobileNumber;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
