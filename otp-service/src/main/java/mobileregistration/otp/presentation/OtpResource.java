package mobileregistration.otp.presentation;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import mobileregistration.otp.presentation.dto.MobileNumberDTO;
import mobileregistration.otp.presentation.dto.OtpGenerationStatusDTO;
import mobileregistration.otp.presentation.dto.OtpValidationRequestDTO;
import mobileregistration.otp.service.OtpService;

//@CrossOrigin
@RestController
@RequestMapping("/otp")
@Api("OTP Controller")
public class OtpResource {

	private static final Logger log = LoggerFactory.getLogger(OtpResource.class);

	@Autowired
	private OtpService service;

	@PostMapping("/generate")
	@ApiOperation(value = "Generate OTP", response = OtpGenerationStatusDTO.class)
	public OtpGenerationStatusDTO generateOtp (@RequestBody @Valid MobileNumberDTO mobileNumberDTO){
		log.info("Generate OTP for {} is received", mobileNumberDTO.getMobileNumber());
		String otp = service.saveOtp(mobileNumberDTO.getMobileNumber());

		OtpGenerationStatusDTO status = new OtpGenerationStatusDTO("Verification code is sent " + otp);

		return status;
	}

	@PostMapping("/validate")
	@ApiOperation(value = "Validate OTP", response = Boolean.class)
	public Boolean validityCheck (@RequestBody @Valid OtpValidationRequestDTO otpCheck){
		log.info("OTP validation check for mobile number {} and code {}", otpCheck.getMobileNumber(), otpCheck.getOtp());
		Boolean validation = service.otpValidation(otpCheck.getMobileNumber(), otpCheck.getOtp());

		return validation;
	}
}