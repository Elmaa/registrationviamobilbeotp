package mobileregistration.otp.utility;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class OtpUtilityService {

	private static final Integer EXPIRE_MINS = 3;
	private LoadingCache<String, Integer> otpCache;

	public OtpUtilityService() {
		super();
		otpCache = CacheBuilder.newBuilder().expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES)
				.build(new CacheLoader<String, Integer>() {
					public Integer load(String key) {
						return 0;
					}
				});
	}

	public int generateOTP(String key) {
		Random random = new Random();
		int otp = 100000 + random.nextInt(900000);
		otpCache.put(key, otp);
		return otp;
	}

	public Optional<Integer> getOtp(String key) {
		Optional<Integer> cachedOTP = Optional.empty();
		
		try {
			cachedOTP = Optional.ofNullable((otpCache.get(key) == 0) ? null : otpCache.get(key));
			return cachedOTP;
		} catch (ExecutionException e) {
			return cachedOTP;
		}
	}

	public void clearOTP(String key) {
		otpCache.invalidate(key);
	}
}
