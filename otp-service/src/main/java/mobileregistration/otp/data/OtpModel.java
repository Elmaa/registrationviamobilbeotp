package mobileregistration.otp.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OtpModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "OTP_MODEL_ID", nullable = false)
	private Long otpModelID;
	private String mobileNumber;
	private Integer otp;
	
	public OtpModel() {
		super();
	}
	
	public OtpModel(String mobileNumber, Integer verificationCode) {
		super();
		this.mobileNumber = mobileNumber;
		this.otp = verificationCode;
	}

	public Long getOtpModelID() {
		return otpModelID;
	}

	public void setOtpModelID(Long userModelID) {
		this.otpModelID = userModelID;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Integer getOtp() {
		return otp;
	}

	public void setOtp(Integer otp) {
		this.otp = otp;
	}

}
