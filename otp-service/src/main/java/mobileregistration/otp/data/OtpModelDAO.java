package mobileregistration.otp.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OtpModelDAO extends JpaRepository<OtpModel, Long>{

}
