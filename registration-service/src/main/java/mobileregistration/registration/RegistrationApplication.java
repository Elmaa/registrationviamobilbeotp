package mobileregistration.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableFeignClients("mobileregistration.registration")
@EnableDiscoveryClient
@EnableSwagger2
public class RegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationApplication.class, args);
	}

	private static final String API_VERSION = "1.0.1";
	private static final String LICENSE_TEXT = "MIT";
	private static final String TITLE = "Mobile OTP Registration";
	private static final String DESCRIPTION = "Mobile OTP Registration";


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(TITLE).version(API_VERSION).description(DESCRIPTION).license(LICENSE_TEXT)
				.build();
	}

	@Bean
	public Docket behzendegiDocket() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("mobileregistration.registration"))
				.paths(PathSelectors.ant("/**")).build();
	}
}
