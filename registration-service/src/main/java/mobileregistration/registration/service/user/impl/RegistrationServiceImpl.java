package mobileregistration.registration.service.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mobileregistration.registration.data.mobileinfo.MobileInfo;
import mobileregistration.registration.data.mobileinfo.MobileInfoDAO;
import mobileregistration.registration.data.user.User;
import mobileregistration.registration.data.user.UserDAO;
import mobileregistration.registration.service.user.RegistrationService;

@Service
public class RegistrationServiceImpl implements RegistrationService{

	@Autowired
	private UserDAO userDao;
	
	@Autowired
	private MobileInfoDAO mobileInfoDao;
	
	@Override
	public void saveMobileNumber(String mobileNumber) {
		MobileInfo mobileInfo = new MobileInfo(mobileNumber);
		mobileInfoDao.save(mobileInfo);
	}

	@Override
	public boolean findByMobileNumber(String mobileNumber) {
		User user = userDao.findByMobileNumber(mobileNumber);
		if(user == null) {
			return false;
		}
		return true;
	}

	@Override
	public void saveUser(String mobileNumber) {
		User user = new User(mobileNumber, mobileNumber);
		userDao.save(user);
	}
}
