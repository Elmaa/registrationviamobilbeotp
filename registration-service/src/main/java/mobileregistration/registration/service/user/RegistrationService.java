package mobileregistration.registration.service.user;

public interface RegistrationService {

	void saveMobileNumber(String mobileNumber);
	boolean findByMobileNumber(String mobileNumber);
	void saveUser(String mobileNumber);
}
