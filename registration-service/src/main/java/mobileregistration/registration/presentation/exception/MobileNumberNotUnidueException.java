package mobileregistration.registration.presentation.exception;

public class MobileNumberNotUnidueException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3587746399647560842L;
	
	public MobileNumberNotUnidueException() {
		super();
	}
	
	public MobileNumberNotUnidueException(String message) {
		super(message);
	}
}
