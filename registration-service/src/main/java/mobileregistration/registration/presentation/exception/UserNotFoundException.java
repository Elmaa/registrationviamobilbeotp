package mobileregistration.registration.presentation.exception;

public class UserNotFoundException extends RuntimeException{
	 
	private static final long serialVersionUID = 5411022878863125573L;

	public  UserNotFoundException() {
		super();
	}
	
	public  UserNotFoundException(String exception) {
		super(exception);
	} 
}
