package mobileregistration.registration.presentation.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler({UserNotFoundException.class, UserAlreadyExistException.class, MobileNumberNotUnidueException.class})
	public ExceptionStatus handleException(RuntimeException ex) {
		ExceptionStatus excpetion = new ExceptionStatus();
		excpetion.setMessage("EXCEPTION ECCURED! " + ex.getMessage());
		
		return excpetion;
	}
	
}
