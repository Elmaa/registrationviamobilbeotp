package mobileregistration.registration.presentation.feign.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mobileregistration.registration.presentation.dto.OtpValiditionDTO;

@FeignClient(name = "validation-check-service")
@RibbonClient(name="validation-check-service")
public interface ValidationServiceProxy {

	@PostMapping("/validation/checkvalidation")
	public Boolean getValidationStatus(@RequestBody OtpValiditionDTO input);

}
