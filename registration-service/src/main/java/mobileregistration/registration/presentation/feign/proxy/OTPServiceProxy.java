package mobileregistration.registration.presentation.feign.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mobileregistration.registration.presentation.dto.VerificationCodeResponseDTO;
import mobileregistration.registration.presentation.dto.VerificationCodeRequestDTO;

@FeignClient(name = "otp-service")
@RibbonClient(name="otp-service")
public interface OTPServiceProxy {

	@PostMapping("/otp/generate")
	public VerificationCodeResponseDTO generateOtp(@RequestBody VerificationCodeRequestDTO userDTO);

}
