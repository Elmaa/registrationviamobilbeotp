package mobileregistration.registration.presentation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import mobileregistration.registration.presentation.dto.OtpValiditionDTO;
import mobileregistration.registration.presentation.dto.VerificationCodeResponseDTO;
import mobileregistration.registration.presentation.exception.UserAlreadyExistException;
import mobileregistration.registration.presentation.feign.proxy.OTPServiceProxy;
import mobileregistration.registration.presentation.feign.proxy.ValidationServiceProxy;
import mobileregistration.registration.presentation.dto.SignUpStatusDTO;
import mobileregistration.registration.presentation.dto.VerificationCodeRequestDTO;
import mobileregistration.registration.service.user.RegistrationService;


@RestController
@RequestMapping("/registration")
@Api("Registration Controller")
public class RegistrationResource {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationResource.class);

	@Autowired
	private RegistrationService registrationService;

	@Autowired
	private OTPServiceProxy proxy;
	
	@Autowired
	private ValidationServiceProxy validationProxy;


	@PostMapping(path = "/mobilenumber")
	@ApiOperation(value = "Verification code generation request", response = VerificationCodeResponseDTO.class)
	public VerificationCodeResponseDTO preSignup(@RequestBody @Valid VerificationCodeRequestDTO input){
		logger.info("Generating verification code request for {}", input.getMobileNumber());
		
		VerificationCodeResponseDTO response = new VerificationCodeResponseDTO();
		if(registrationService.findByMobileNumber(input.getMobileNumber())) {
			throw new UserAlreadyExistException("User is already exist!");
		}
		else {
			registrationService.saveMobileNumber(input.getMobileNumber());
			
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);

			Map<String, Object> uriVariables = new HashMap<>();
			uriVariables.put("mobileNumber", input.getMobileNumber());

			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(uriVariables, headers);

			ResponseEntity<VerificationCodeResponseDTO> responseEntity = new RestTemplate().postForEntity(
					"http://localhost:8100/registration/mobilenumber-feign", entity, VerificationCodeResponseDTO.class);

			response = responseEntity.getBody();
		}
		return response;
	}

	@PostMapping(path = "/signup")
	@ApiOperation(value = "Sign up user", response = SignUpStatusDTO.class)
	public SignUpStatusDTO signUp(@RequestBody @Valid OtpValiditionDTO input){

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);

		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("mobileNumber", input.getMobileNumber());
		uriVariables.put("otp", input.getOtp());

		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(uriVariables, headers);

		ResponseEntity<Boolean> responseEntity = new RestTemplate().postForEntity(
				"http://localhost:8100/registration/signup-feign", entity, Boolean.class);

		Boolean response = responseEntity.getBody();

		if(response) {
			registrationService.saveUser(input.getMobileNumber());
			return new SignUpStatusDTO(input.getMobileNumber(), "Welcome");
		}

		return new SignUpStatusDTO("", "Verification code is wrong");
	}

	@PostMapping("/mobilenumber-feign")
	public VerificationCodeResponseDTO sendMobileNumberFeign(@RequestBody VerificationCodeRequestDTO input) {

		VerificationCodeResponseDTO response = proxy.generateOtp(input);

		logger.info("Request is sent to Verification Service");

		return response;
	}

	@PostMapping("/signup-feign")
	public Boolean getValidationStatus(@RequestBody OtpValiditionDTO input) {

		Boolean response = validationProxy.getValidationStatus(input);

		logger.info("Request is sent to Verification Service");

		return response;
	}
}
