package mobileregistration.registration.presentation.dto;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

public class VerificationCodeRequestDTO {

	@NotBlank
	@Length(min=11, max=11, message = "Mobile number should be 11 characters")
	private String mobileNumber;
	
	public VerificationCodeRequestDTO() {
		super();
	}
	
	public VerificationCodeRequestDTO(String username) {
		super();
		this.mobileNumber = username;
	}
	
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
