package mobileregistration.registration.presentation.dto;

public class ExceptionDTO extends DTO{

	private String message;
	
	public ExceptionDTO() {}
	
	public ExceptionDTO(String status, String message) {
		this.setStatus(status);
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
