package mobileregistration.registration.presentation.dto;

public class SignUpStatusDTO {

	private String userName;
	private String status;

	public SignUpStatusDTO(String userName, String status) {
		super();
		this.userName = userName;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
