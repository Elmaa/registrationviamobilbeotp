package mobileregistration.registration.presentation.dto;

public class OtpValiditionDTO {

//	@NotBlank
//	@Length(min=11, max=11, message = "Mobile number should be 11 characters")
	private String mobileNumber;
	
//	@NotBlank
	private Integer otp;


	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Integer getOtp() {
		return otp;
	}
	public void setOtp(Integer otp) {
		this.otp = otp;
	}
}
