package mobileregistration.registration.presentation.dto;

public class VerificationCodeResponseDTO {

	private String status;

	public VerificationCodeResponseDTO() {
		super();
	}
	
	public VerificationCodeResponseDTO(String status) {
		super();
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
