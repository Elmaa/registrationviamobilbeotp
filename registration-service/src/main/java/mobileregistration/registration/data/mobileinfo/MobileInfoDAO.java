package mobileregistration.registration.data.mobileinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MobileInfoDAO extends JpaRepository<MobileInfo, Long>{

}
