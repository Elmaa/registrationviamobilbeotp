package mobileregistration.registration.data.mobileinfo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MobileInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MOBILE_INFO_ID", nullable = false)
	private Long mobileInfoID;
	
	@Column(name = "MOBILE_NUMBER" , nullable = false)
	private String mobileNumber;

	@Column(name = "JOIN_IN_DATE", nullable = false)
	private Date joinInDate;
	
	public MobileInfo() {
		super();
	}
	
	public MobileInfo(String mobileNumber) {
		super();
		this.mobileNumber = mobileNumber;
		this.joinInDate = new Date();
	}

	public Long getMobileInfoID() {
		return mobileInfoID;
	}
	
	public void setMobileInfoID(Long mobileInfoID) {
		this.mobileInfoID = mobileInfoID;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public Date getJoinInDate() {
		return joinInDate;
	}

	public void setJoinInDate(Date joinInDate) {
		this.joinInDate = joinInDate;
	}
}
