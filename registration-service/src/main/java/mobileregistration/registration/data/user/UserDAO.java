package mobileregistration.registration.data.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<User, Long>{

	User findByMobileNumber(String mobileNumber);
}
