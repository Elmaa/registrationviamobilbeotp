package mobileregistration.registration.data.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID", nullable = false)
	private Long userID;
	
	@Column(name = "USER_NAME" , nullable = false)
	private String userName;
	
	@Column(name = "MOBILE_NUMBER" , nullable = false, unique=true)
	private String mobileNumber;

	@Column(name = "JOIN_IN_DATE", nullable = false)
	private Date joinInDate;
	
	public User() {
		super();
	}
	
	public User(String userName, String mobileNumber) {
		super();
		this.userName = userName;
		this.mobileNumber = mobileNumber;
		this.joinInDate = new Date();
	}
	
	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public Date getJoinInDate() {
		return joinInDate;
	}

	public void setJoinInDate(Date joinInDate) {
		this.joinInDate = joinInDate;
	}
}
