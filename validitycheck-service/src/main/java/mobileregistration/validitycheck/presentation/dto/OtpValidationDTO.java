package mobileregistration.validitycheck.presentation.dto;

public class OtpValidationDTO {

	private String mobileNumber;
	private Integer otp;
	
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Integer getOtp() {
		return otp;
	}
	public void setOtp(Integer otp) {
		this.otp = otp;
	}
}
