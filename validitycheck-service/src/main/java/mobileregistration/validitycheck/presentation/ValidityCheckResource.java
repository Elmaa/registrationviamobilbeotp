package mobileregistration.validitycheck.presentation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import mobileregistration.validitycheck.presentation.dto.OtpValidationDTO;
import mobileregistration.validitycheck.presentation.feign.proxy.OtpServiceProxy;

@RestController
@RequestMapping("/validation")
@Api("OTP validity check middleware controller")
public class ValidityCheckResource {

	private static final Logger logger = LoggerFactory.getLogger(ValidityCheckResource.class);

	@Autowired
	private OtpServiceProxy proxy;

	@PostMapping(path = "/checkvalidation")
	@ApiOperation(value = "Validity check of requested OTP", response = Boolean.class)
	public Boolean checkValidation(@RequestBody OtpValidationDTO input){

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);

		Map<String, Object> uriVariables = new HashMap<>();
		uriVariables.put("mobileNumber", input.getMobileNumber());
		uriVariables.put("otp", input.getOtp());

		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(uriVariables, headers);

		ResponseEntity<Boolean> responseEntity = new RestTemplate().postForEntity(
				"http://localhost:9100/validation/checkvalidation-feign", entity, Boolean.class);

		Boolean response = responseEntity.getBody();
		return response;
	}


	@PostMapping("/checkvalidation-feign")
	public Boolean checkValidationStatus(@RequestBody OtpValidationDTO input) {

		logger.info("Request is sent from Validity check service to OTP Service");

		Boolean response = proxy.getValidationStatus(input);

		return response;
	}
}
