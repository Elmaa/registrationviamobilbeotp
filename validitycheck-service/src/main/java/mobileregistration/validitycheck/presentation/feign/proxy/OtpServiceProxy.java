package mobileregistration.validitycheck.presentation.feign.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mobileregistration.validitycheck.presentation.dto.OtpValidationDTO;

@FeignClient(name = "otp-service")
@RibbonClient(name="otp-service")
public interface OtpServiceProxy {

	@PostMapping("/otp/validate")
	public Boolean getValidationStatus(@RequestBody OtpValidationDTO input);
}
